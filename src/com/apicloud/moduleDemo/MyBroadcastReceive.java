package com.apicloud.moduleDemo;

import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;
import org.json.JSONException;
import org.json.JSONObject;

public class MyBroadcastReceive extends UZModule {
	private UZModuleContext mJsCallback;
	static final int ACTIVITY_REQUEST_CODE_B = 100;

	public MyBroadcastReceive(UZWebView webView) {
		super(webView);
	}

	public void jsmethod_getBroadcastReceiverForResult(
			UZModuleContext moduleContext) {
		this.mJsCallback = moduleContext;
		android.content.Intent intent = new android.content.Intent(context(),
				MyActivity.class);
		startActivityForResult(intent, 100);
	}

	public void onActivityResult(int requestCode, int resultCode,
			android.content.Intent data) {
		if ((resultCode == -1) && (requestCode == 100)) {
			String result = data.getStringExtra("data");
			if ((result != null) && (this.mJsCallback != null)) {
				try {
					JSONObject ret = new JSONObject();
					ret.put("data", result);
					this.mJsCallback.success(ret, true);
					this.mJsCallback = null;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void onClean() {
		if (this.mJsCallback != null) {
			this.mJsCallback = null;
		}
	}
}
