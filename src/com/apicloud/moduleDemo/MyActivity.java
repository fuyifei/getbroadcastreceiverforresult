package com.apicloud.moduleDemo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

public class MyActivity extends Activity {
	private SMSReceiver smsReceiver = null;
	private static final String SMS_ACTION = "com.scanner.broadcast";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TimeOutThread timeOut = new TimeOutThread();
		timeOut.start();
		this.smsReceiver = new SMSReceiver();

		IntentFilter filter = new IntentFilter();

		filter.addAction(SMS_ACTION);

		registerReceiver(this.smsReceiver, filter);
		Log.e(null, "注册");
		
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode != 139) {
				//除扫描键 任意键 主动停止监听
				Intent resultData = new Intent();
				resultData.putExtra("data", "timeOut");
				setResult(-1, resultData);
				finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(this.smsReceiver);
		Log.e(null, "取消注册");
	}

	class SMSReceiver extends BroadcastReceiver {
		SMSReceiver() {
		}

		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			String datastr = String.valueOf(bundle.get("data"));
			Intent resultData = new Intent();
			resultData.putExtra("data", datastr);
			MyActivity.this.setResult(-1, resultData);
			MyActivity.this.finish();
		}
	
	}
	class TimeOutThread extends Thread{
		@Override
		public void run() {
			try {
				//10秒后 发送 “timeOut”到前端
				Thread.sleep(10000);
				Intent resultData = new Intent();
				resultData.putExtra("data", "timeOut");
				MyActivity.this.setResult(-1, resultData);
				MyActivity.this.finish();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
